use crate::nat::*;
use std::marker::PhantomData;
use std::mem;

#[derive(Debug)]
pub struct Vector<A, N: Nat> {
    elems: Vec<A>,
    _marker: PhantomData<N>
}

pub fn new_vec<A>() -> Vector<A, Zero> {
    Vector {
        elems: Vec::new(),
        _marker: PhantomData
    }
}

pub fn append<A, N>(vector: Vector<A, N>, elem: A) -> Vector<A, Succ<N>>
    where N: Nat, A: Clone {
    let mut e = vector.elems.to_vec();
    e.push(elem);
    let result: Vector<A, Succ<N>> = Vector {
        elems: e,
        _marker: PhantomData
    };

    result
}

pub fn pop<A, N>(vector: Vector<A, Succ<N>>) -> (Vector<A, N>, Option<A>)
where N: Nat, A: Clone {
    let mut new_elems = vector.elems.to_vec();
    let e = new_elems.pop();
    let result: Vector<A, N> = Vector {
        elems: new_elems,
        _marker: PhantomData
    };

    (result, e)
}

pub trait Intable {
    fn to_u32(&self) -> u32;
    fn from_u32(i: u32) -> Self;
}

impl Intable for u32 {
    fn to_u32(&self) -> u32 {
        *self
    }

    fn from_u32(i: u32) -> Self {
        i
    }
}

pub fn combine<A, N>(
    v1: Vector<A, N>,
    v2: Vector<A, N>
) -> Vector<A, N>
where N: Nat, A: Clone + Intable {
    let mut new_elems: Vec<A> = Vec::new();
    for i in 0..v1.elems.capacity() {
        let tmp_res = v1.elems.get(i).unwrap().to_u32() + v2.elems.get(i).unwrap().to_u32();
        new_elems.push(Intable::from_u32(tmp_res));
    }

    Vector {
        elems: new_elems,
        _marker: PhantomData
    }
}


impl<A: Clone> Vector<A, Zero> {
    pub fn new() -> Vector<A, Zero> {
        Vector {
            elems: Vec::new(),
            _marker: PhantomData
        }
    }
    pub fn append(self: &mut Vector<A, Zero>, elem: A) -> &mut Vector<A, Succ<Zero>> {
        let mut e = self.elems.to_vec();
        e.push(elem);
        self.elems = e;
        unsafe { mem::transmute(self) }
    }
}

impl<A: Clone, N: Nat> Vector<A, Succ<N>> {
    pub fn append(self: &mut Vector<A, Succ<N>>, elem: A) -> &mut Vector<A, Succ<Succ<N>>> {
        let mut e = self.elems.to_vec();
        e.push(elem);
        self.elems = e;
        unsafe { mem::transmute(self) }
    }
}
