use crate::reify::*;

pub struct True;
pub struct False;
pub trait Bool {}

impl Bool for True {}
impl Bool for False {}

impl Reifiable<bool> for True {
    fn reify() -> bool {
        true
    }
}

impl Reifiable<bool> for False {
    fn reify() -> bool {
        false
    }
}

trait And<T: Bool> {
    type Result: Bool;
}

impl And<True> for True {
    type Result = True;
}

pub trait Or<T: Bool>: Bool {
    type Result;
}

impl Or<True> for True {
    type Result = True;
}

impl Or<True> for False {
    type Result = True;
}

impl Or<False> for True {
    type Result = True;
}
