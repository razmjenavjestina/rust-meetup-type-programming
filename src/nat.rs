use crate::boolean::*;
use crate::reify::*;

#[derive(Debug)]
pub struct Zero;
#[derive(Debug)]
pub struct Succ<T: Nat>(T);

pub trait Nat {}

impl Nat for Zero {}
impl <T: Nat> Nat for Succ<T> {}


impl Reifiable<u32> for Zero {
    fn reify() -> u32 {
        0
    }
}

impl<A> Reifiable<u32> for Succ<A>
where A: Nat + Reifiable<u32> {
    fn reify() -> u32 {
        1 + A::reify()
    }
}


pub trait Add<Rhs: Nat>: Nat {
    type Result: Nat;
}

impl <T: Nat> Add<T> for Zero {
    type Result = T;
}

impl<A: Nat, B: Nat> Add<B> for Succ<A>
where A: Add<Succ<B>> {
    type Result = < A as Add<Succ<B>> >::Result;
    // A = 3
    // B = 5
    // Succ(3) = 4
    // impl Add<5> for 4 {
    //     type Result = 3 + 5
    //                 = 2 + 6
    //                 = 1 + 7
    //                 = 0 + 8
    //                 = 8
    // }
}

// Multiplication

pub trait Mult<Rhs: Nat>: Nat {
    type Result: Nat;
}

impl <Rhs: Nat> Mult<Rhs> for Zero {
    type Result = Zero;
}

impl <A, B> Mult<A> for Succ<B> where
    A: Nat,
    B: Nat + Mult<A>,
    A: Add<<B as Mult<A>>::Result> {
    type Result = <A as Add<<B as Mult<A>>::Result>>::Result;
    // A = 3
    // B = 5
    // Succ(5) = 5
    // impl Mult<3> for Succ(6)
    //     type Result = 6 * 3
    //                 = 3 + (5 * 3)
    //                 = 3 + (3 + (4 * 3))
    //                 = 3 + (3 + (3 + (3 * 3))
    //                 = 3 + (3 + (3 + (3 + (2 * 3)))
    //                 = 3 + (3 + (3 + (3 + (3 + (1 * 3)))
    //                 = 3 + (3 + (3 + (3 + (3 + (3 + 0)))
    //                 = (koristeci Add) = 18
}



pub trait Even {
    type Result;
}

impl Even for Zero {
    type Result = True;
}

impl Even for Succ<Zero> {
    type Result = False;
}

impl<T: Nat + Even> Even for Succ<Succ<T>> {
    type Result = <T as Even>::Result;
}
