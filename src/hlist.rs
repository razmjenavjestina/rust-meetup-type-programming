use std::marker::PhantomData;

use crate::expr::*;
use crate::nat::*;
use crate::boolean::*;

struct HCons<Head, Tail> {
    h: PhantomData<Head>,
    t: PhantomData<Tail>
}
struct HNil;

impl<Head: Expr, Tail: Expr> Expr for HCons<Head, Tail> {
    type Result = HCons<Head, Tail>;
}

trait IsPrime {
    type Result;
}

impl IsPrime for Succ<Zero> {
    type Result = True;
}


trait Factorize {
    type Result;
}

impl Factorize for Zero {
    type Result = HCons<Zero, HNil>;
}
