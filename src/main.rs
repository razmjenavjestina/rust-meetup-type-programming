
mod nat;
mod boolean;
mod reify;
mod equality;
mod expr;
mod vector;
mod hlist;

use nat::*;
use boolean::*;
use expr::*;
use equality::*;
use reify::*;
use vector::*;

fn main() {
    type One = Succ<Zero>;
    type Two = Succ<One>;
    type Three = Succ<Two>;
    type Four = Succ<Three>;
    type Five = Succ<Four>;
    type Six = <Three as Mult<Three>>::Result;

    println!("Zero = = {:?}", Zero::reify());
    println!("type One = Succ<Zero> = {:?}", One::reify());
    println!("type Two = Succ<One> = {:?}", Two::reify());
    println!("type Three = Succ<Two> = {:?}", Three::reify());
    println!("type Four = Succ<Three> = {:?}", Four::reify());
    println!("type Five = Succ<Four> = {:?}", Five::reify());
    println!("type Six = <Three as Mult<Three>>::Result = {:?}", Six::reify());

    type E1 = <True as Equal<True>>::Result;
    type E2 = <Four as Equal<Two>>::Result;
    type E3 = <<Three as Even>::Result as Equal<False>>::Result;
    type E4 = <
            <Three as Even>::Result
            as Or<
                    <Three as Even>::Result
                    >
               >::Result;
    println!("E1 = {:?}", E1::reify());
//    println!("E2 = {:?}", E2::reify());
    println!("E3 = {:?}", E3::reify());
//    println!("E4 = {:?}", E4::reify());

    type R1 = <True as If<True, False>>::Result;
    type R2 = <False as If<Succ<Zero>, Zero>>::Result;
    println!("R1 = {:?}", R1::reify());
    println!("R2 = {:?}", R2::reify());

    let v: Vector<u32, Zero> = new_vec();
    let v1 = append(v, 3);
    let v2 = append(v1, 4);

    println!("{:?}", v2);

    let a = new_vec();
    let a1 = append(a, 100);
    let a2 = append(a1, 200);
    println!("{:?}", a2);

    let res = combine(a2, v2);
    println!("{:?}", res);

    let (v3, x) = pop(res);
    println!("Removed {:?} from {:?}", x, v3);
    let (v4, y) = pop(v3);

    let mut b = Vector::new();
    b.append(2).append(5).append(4);
    println!("Mutable: {:?}", b);
}


