use crate::boolean::*;
use crate::nat::*;

pub trait Expr {
    type Result;
}

impl Expr for True {
    type Result = True;
}
impl Expr for False {
    type Result = False;
}
impl Expr for Zero {
    type Result = Zero;
}
impl<T: Nat> Expr for Succ<T> {
    type Result = Succ<T>;
}

pub trait If<C, B>
where C: Expr,
      B: Expr {
    type Result;
}

impl<C : Expr, B: Expr> If<C, B> for True {
    type Result = C;
}

impl<C : Expr, B: Expr> If<C, B> for False {
    type Result = B;
}

