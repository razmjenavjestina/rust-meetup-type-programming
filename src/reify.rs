pub trait Reifiable<T> {
    fn reify() -> T;
}

// Nat -> u32
// Boolean -> bool
