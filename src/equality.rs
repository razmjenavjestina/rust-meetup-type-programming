use crate::boolean::*;
use crate::nat::*;

pub trait Equal<A> {
    //    default types are not in rust stable yet
    // type Result = True;
    type Result: Bool;
}

impl Equal<False> for False {
    type Result = True;
}
impl Equal<True> for True {
    type Result = True;
}
impl<T: Nat> Equal<T> for T {
    type Result = True;
}


impl Equal<True> for False {
    type Result = False;
}

impl Equal<False> for True {
    type Result = False;
}



